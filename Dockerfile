FROM ruby:2.6

RUN apt-get update -qq
RUN apt-get install -y apt-utils vim

RUN mkdir /cooksey.io
WORKDIR /cooksey.io

COPY . /cooksey.io

RUN bundle install

# Build site
RUN bundle exec middleman build

RUN ln -s /usr/share/nginx/html /cooksey.io/build
