#!/bin/bash

if [[ "$(docker images -q cooksey_io_build 2> /dev/null)" == "" ]]; then
  docker build -t cooksey_io_build .
fi

docker create -ti --name cooksey_io_build cooksey_io_build bash

docker cp cooksey_io_build:/cooksey.io/build.tar.bz2 .

docker rm -fv cooksey_io_build

bunzip2 build.tar.bz2
tar -xvf build.tar

echo "Deployed $(git describe)"
