---

title: From GitHub to GitLab
date: 2018-06-04 14:10 MDT
tags: github,gitlab,microsoft,linux

---

I, like many other developers this week, have migrated all of my git repositories from [GitHub](https://github.com) to
[GitLab](https://gitlab.com). 

I had never looked at GitLab with any real vigor, but upon the Micorsoft acquisition was announced, I sought out to learn
the differences between GitHub and GitLab.  I was happily surprised to see many paid GitHub features being offered from on GitLab.
The baked in CI/CD stuff looks great and there's no paid restrictions on private repos.

I don't want to jump on the Microsoft hate train. There are plenty of passengers on it already. But, I can't trust Microsoft.

It's true that Microsoft has come a long way in the [FOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software) community,
but it's impossible for me to forget that they spent decades and millions waging war on Linux and open source.  Releasing an
admittedly fantastic [TypeScript](https://www.typescriptlang.org/) and some IDE doesn't absolve them of the past.

I do believe it could have been much worse.  There's no telling what Oracle or IBM would have done to our beloved GitHub, but
that small relief only goes so far.  I simply don't trust Microsoft enough to run GitHub regarding privacy, data collection,
the spirit of open source, and who owns my code.

Migrating to GitLab was as easy as linking [my new GitLab account](https://gitlab.com/Cooksey/) to my GitHub account and clicking
the "import all repositories" button. 

This post is meant to serve as the first commit on GitLab as well as the first deploy.
