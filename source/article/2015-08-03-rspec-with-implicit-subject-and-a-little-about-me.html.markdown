---
title: RSpec With Implicit Subject & A Little About Me
date: 2015/08/03
tags: charlie cooksey, ruby, rails, rspec, implicit subject
---
This is the first post I've made with Jekyll.  I'm still deciding if I like it; but, I think it's database-less state along with the markdown entries make this a winner for me. The Jeykll framework is pretty impressive.

I guess I should say some things for my first post.  Well, I'm a software developer at Rackspace Hosting, Inc. 

Tools / languages that I use:

* Ruby
* Rspec
* Rails
* vim
* RubyMine
* Rack
* PHP
* PHPStorm
* Symfony 1.4 & 2.x
* PHPUnit
* JavaScript
* CoffeeScript
* AngularJS
* NodeJS
* jQuery
* Python
* Tornado

There are other things, but I'm really just trying out markdowns functionality here rather than cloning my [LinkedIn](https://www.linkedin.com/in/charliecooksey) page. So, I will stop there. :) 

I will try to begin blogging about technical things. I will probably start with test practices. I guess I could try out some of markdown's syntax highlighting real quick:

This is an example of when I like to use implicit subjects -- a somewhat controversial subject. I prefer implicit subjects when the test has no need to be complex. This use case is straight-forward and still easy to read with implicit subject. I think in these cases, using the implicit subject gives rspec the better (more readable) one-liner syntax.

```ruby
    require 'spec_helper'
    module App::Handler
      describe TagsHandler do

        let(:handler) { TagsHandler.new }

        describe "#has_tags?" do
          subject { handler.has_tags?(ticket_data) }
          let(:ticket_data) { { 'tags' => [] } }

          context "when a tags key is set" do
            it { should be_truthy }
          end

          context "when a tags key is not set" do
            let(:ticket_data) { { 'not_tags' => [] } }
            it { should be_falsey }
          end
        end

        describe "#split_tags" do
          subject { handler.split_tags(tags_data) }
          let(:tags_data) { "foo, bar, blah" }

          context "with comma separated string" do
            it { should eql(tags_data.split(',')) }
            it { should be_kind_of(Array) }
          end

          context "with an empty array of tags" do
            let(:tags_data) { [] }
            it { should eql([]) }
            it { should be_kind_of(Array) }
            it { should be_empty }
          end

          context "with an empty string of tags" do
            let(:tags_data) { "" }
            it { should eql([]) }
            it { should be_kind_of(Array) }
            it { should be_empty }
          end

          context "with a nil tags value" do
            let(:tags_data) { nil }
            it { should eql([]) }
            it { should be_kind_of(Array) }
            it { should be_empty }
          end
      end
    end
```
