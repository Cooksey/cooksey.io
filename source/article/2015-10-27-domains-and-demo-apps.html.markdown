---

title: Domains and Demo Apps
date: 2015/10/27
tags: charlie, charles, cooksey, ssh, angular, angularjs, angular2, demo app
---
It's been a pretty long while since I've posted anything on here. I've been meaning to, but I keep forgetting.

I've recently left my software developer position after two years with Rackspace to re-join my old company.

A coworker and I recently got back from a meeting at the headquarters in Little Rock, Arkansas. Hurricane Patricia ended up cancelling my flight home Friday, and I ended up stuck in Little Rock a day longer than planned. While exhausting catching a 6:20 AM flight, it was a good time. 

Anyway. The reason for this post is mostly so I don't have to SSH into my server anymore to remember the URL to an app I've deployed.

A year ago or so, I was asked an interview question – to provide a working app based on said question. I was never pleased with what I sent them. So, I did it better and deployed it.

It's an [AngularJS](https://angularjs.org) app.  The code can be found in my [cdcooksey/airplane-algorithm repo](https://github.com/cdcooksey/airplane-algorithm). 

The demo is usable at [airplane.cooksey.io](http://airplane.cooksey.io).

I plan to also deploy a few other apps on [cooksey.io](http://cooksey.io), although right now it's just the one app. I'll probably move this blog there soon.
