---

title: No More Nomad Life
date: 2016/10/08
tags: family, denver, colorado, pictures, prue, charlie, cooksey

---

This is not a coding post, so this is a bit off topic, but I don't care:

One of these days, I'm going to actually update the UI to this thing.  I've been extraordinarily lazy on that front.

Anyway.

My wife and I recently bought our first home here in Denver, Colorado.

In the last 7 years, we've lived in Australia, Little Rock, San Antonio, and Denver.

We've spent our entire marriage living nomadically all over the southern United States.  I love the south.  I was born and raised in the south.  My entire family lives in the south.  But, I've always wanted to move up north (Colorado is "up north" to me).

I've always wanted snow, mountains, and pine trees. And, it didn't hurt that two wonderful friends we made while I worked at Rackspace live in Denver, CO.

So, we decided to put down some roots a few months ago.  We closed on our house two months ago.

![My helpful screenshot]({{ site.url }}/assets/2016-10-charlie-house-1.jpg)