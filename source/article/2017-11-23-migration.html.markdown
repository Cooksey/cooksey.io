---

title: Migration
date: 2017/11/23
tags: Ruby, Middleman, Rails, Charlie, Cooksey, Danueil

---

From Jeylkk to Middleman

Welp, I've been using Jeykll for quite a long time now, but I find it just doesn't have enough
flexibility for me to actually keep it updated.

Today I am hoping that changes.  I'm migrating blog.cooksey.io to cooksey.io and fleshing out a real
vanity site.
