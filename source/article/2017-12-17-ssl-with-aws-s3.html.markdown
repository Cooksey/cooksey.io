---

title: SSL with AWS S3
date: 2017/12/17
tags: SSL, AWS, AWS S3, Namecheap, Midleman, Static Site, SSL certificate, certificate, Sebastian Buckpesch

---

I've finally completed the move from Jeykll to Middleman, and migrated the old blog to [cooksey.io](https://www.cooksey.io) on
AWS S3.

I've started to fill out the landing page, but it was a little embarrassing that I still hadn't set up SSL. You might say,
why do you care about SSL on a static site that takes no user input?  Well, a lot of reasons, but th most important reason
was that not having it bothered me.

I followed [a helpful article by Sebastian Buckpesch](https://medium.com/@sbuckpesch/setup-aws-s3-static-website-hosting-using-ssl-acm-34d41d32e394), though
some of the AWS stuff has changed since his article, and I didn't need to use Route 53 for email.

Installing SSL is usually pretty trivial, but it turns out that it isn't very trivial with hosting on AWS's S3!

Luckily, [Namecheap](https://namecheap.com), my domain and DNS provider -- because screw GoDaddy, offers straight-forward
email forwarding that allowed me to prove ownership of [cooksey.io](https://www.cooksey.io) in AWS Certificate Manager.

After getting the SSL cert, I was able to use AWS CloudFront for two benefits:

* Distribute the site across AWS's datacenter to help load speed and add redundancy
* Get a CloudFront URL that covers all datacenter's and allows me to create a CNAME DNS record

I'm still really happy with CodeShip + AWS CLI being my CI/CD deploy pipeline.  This post will be the first merge to
master since setting up SSL and CloudFront.

Hopefully this post will serve as a successful test that my deploy pipeline still works after moving to CloudFront.

We'll see in about 15 minutes. :)

I hope you're all having a good time with the holidays and what not!
