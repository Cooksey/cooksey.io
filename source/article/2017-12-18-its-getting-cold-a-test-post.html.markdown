---

title: It's getting Cold (A Test Post)
date: 2017/12/18
tags: Denver, cold, Cloudfront, cache expiration

---

It's getting cold leading up to Christmas. 

<img src="/article/2017/12/18/its-getting-cold-a-test-post/weather.png"
     alt="Denver weather for Christmas 2017"
     width="80%"
     />

This post is really just a test to see how long it takes Cloudfront to automatically expire the S3 cache
and distribute new content.
