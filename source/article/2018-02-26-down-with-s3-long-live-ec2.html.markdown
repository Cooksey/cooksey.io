---

title: Down With S3, Long Live EC2
date: 2018/02/26
tags: AWS, S3, EC3, static hosting, middleman, ruby, limited scope
description: moving from Amazon AWS S3 to EC2
---

I've ditched hosting the new static site on AWS S3.  It's mostly an impatient thing, but I've also
seen odd metrics between the AWS S3 analytics and Google Analytics.

AWS S3 is insanely reliable, but it also publishes page changes on a cache timer.  This is annoying when
you're update your goofy new vanity site and have to manually invalidate the S3 cache so your changes
appear on the domain without delay.

And, the metrics between AWS S3 and Google have been remarkably different.  I'm not really sure what to
even make of the difference.

I've been trying to get a new account on [New Relic](https://www.newrelic.com), but their sign up page is being weird.

Well, really I'm only posting this to test, yet again, the new EC2 deployment cycle, so you'll forgive me for ending
this discussion abruptly.

I don't have a clever way to warp this up, but regarding programming, I can offer you this recommendation: read
[this book, Refactoring: Ruby Edition: Ruby Edition.](https://smile.amazon.com/Refactoring-Ruby-Addison-Wesley-Professional/dp/0321984137/)

On one last note, I'm finding `RubyClass#call` [docs](https://ruby-doc.org/core-2.2.0/Proc.html#method-i-call) more useful lately.

I've been finding it useful for refactoring large classes into singluarly purposed stateless classes. The same result could be achieved
by using class methods, but I find `#call` lends itself to singular thinking, limiting the scope of the class.

```ruby
module Report
  class ProductsByDateRange
    class << self # because why not?
      def call(date_range)
        Product
         .where(start_date: date_range.range.start_date)
         .where(end_date: date_range.end_date)
      end
    end
  end
end
```

Maybe a better approach for this specific example would be to extend the `Product` model and put the query in
`ProductsbyDateRange < Product` where `Product < ActiveRecord::Base`.

If the routine in question is just a database call, I'll probably extend the `ActiveRecord` model.  But, if the
routine is a loop, sum, or any other business logic, I'll probably use this `#call` style.