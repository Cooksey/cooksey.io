---

title: There's more to learning to fish than buying a pole.
date: 2018-07-01 09:14 MDT
tags: fishing, fishing pole, trout, ambitions, stupidity

---

I've been learning to fish. I've learned this is, in fact, not how it works.

<img src="/article/2018/07/01/there-s-more-to-learning-to-fish-than-buying-a-pole/learning_to_fish.png"
     alt="It turns out that there's more to fishing than buying a pole"
     width="80%"
     />
