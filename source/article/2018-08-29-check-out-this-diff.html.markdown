---
title: Check Out This Diff
date: 2018/08/29
tags: ruby, rails, git, diff
---

Oh, snap. Look! I had already refactored this controller quite a lot, but then I did this. So satisfying.

<img alt="image of readable one-liner in ruby controller rails"
     width="80%"
     src="/article/2018/08/29/check-out-this-diff/diff.jpeg" />
