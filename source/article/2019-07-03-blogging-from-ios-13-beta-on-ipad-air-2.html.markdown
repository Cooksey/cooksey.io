---
title: Blogging from iOS 13 beta on iPad Air 2
date: 2019/07/03
tags: ruby, php, git, ipad, ios 13 
---

The introduction of iPadOS looked promising enough for me to consider replacing
more of the work I do on my [Arch](https://www.archlinux.org) on my laptop. I'm
not interested in deprecating a laptop that I love, but offloading more simple
tasks to other, more mobile, devices appeals to my sense of freedom.

This morning I downloaded the
[Pretext](https://apps.apple.com/us/app/pretext/id1347707000) app for editing
[Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#links)
on iPad. It seems to do the job well.

My job gives me a MacBook Pro for development, and I have a personal Dell XPS
laptop running Arch Linux. For reasons not quite known to me, I prefer to do as
much work as I can on my phone or tablet rather than my laptops. I like to
think the reason is that I feel more able to accomplish tasks without the need
of a proper computer, but the reality is more likely that I just like gadgets.

As a final test of this iPad blogging experience, let's see how a little code
does.

<script
src="https://gist.github.com/cdcooksey/2f9116324ebb5e6b170728db50969cb7.js"></script>

Seems good. PHP for funsies?

<script
src="https://gist.github.com/cdcooksey/3bf89ae9cddec1acc57dabcb28d827e4.js"></script>

Well, that seems to work fine. Now I'm thinking about how I might send this
markdown file from my iPad
[MNV62LL/A](https://www.cnet.com/products/apple-ipad-air-2-wi-fi-tablet-32-gb-9-7-mnv62lla/)
to automatically insert the file into my git repository. Maybe I will set up a
secret email address that I can send this to. A little bit of code could watch
the email, pull new markdown files into the repo, rebuild
[middleman](https://middlemanapp.com) and automatically push the addition to
AWS through the existing CI/CD pipeline.

We'll see.
