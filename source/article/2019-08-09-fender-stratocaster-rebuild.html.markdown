---
title: Fender Stratocaster Rebuild
date: 2019/08/09
tags: fender, squier, stratocaster, japan
---

Yesterday I started writing my first song in thirteen years.

<img alt="This guitar is a 1987 Japanese Fender Stratocaster Squier"
     width="80%"
     src="/article/2019/08/09/fender-stratocaster-rebuild/stratocaster-00.jpeg" />

This guitar is an E-Series 1987 Japanese Fender Stratocaster Squier. I didn't
know most of that until a month ago. At thirteen, I got it from a pawn shop in
small Texas town. It was my first guitar. Many have come and gone over the
years, but this one has been with me since. This guitar moves with me to
Australia and back. It has my companion and tool in six bands that I can recall
from memory right now. It was never great, but that was never the point of this one, for me.

It didn't even work anymore. It was missing the high E tuning key, the
electronics hardly worked, the fret board had sharp pieces. I had been
considering buy a new electric guitar for about a year and had recently gotten
very serious about it. However, my wife talked me into fixing the Stratocaster
up, and I'm really happy with how it turned out.

<img alt="old, broken 1987 Japanese Fender Stratocaster Squier"
     width="80%"
     src="/article/2019/08/09/fender-stratocaster-rebuild/stratocaster-01.jpeg" />

It was in a sad state. And nasty. Look at the bridge. Yikes

<img alt="electronics Japanese Fender Stratocaster Squier"
     width="80%"
     src="/article/2019/08/09/fender-stratocaster-rebuild/stratocaster-02.jpeg" />

I needed to drill new holes in the headstock to install the new Fender tuning
keys.  After that, I used steel wool to smooth the fret board and a very fine
grain sandpaper to blunt sharp frets.  Third, I wired inn the electronics.
Then, I gave the bridge components a thorough cleaning.

<img alt="electronics Japanese Fender Stratocaster Squier"
     width="80%"
     src="/article/2019/08/09/fender-stratocaster-rebuild/stratocaster-03.jpeg" />

The last thing to do was bolt everything in and back together and string it up.

The pickups I went with are Seymour Duncan —

* Neck: Little 59 humbucker
* Middle: split single coil duckbucker
* Bridge: JB Jr. humbucker

Here's an unlisted YouTube video of how it sounds. In this video, I'm playing a
summary of 46 & 2 by Tool.

<iframe width="560" height="315" src="https://www.youtube.com/embed/fVw4DIFWjJw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
